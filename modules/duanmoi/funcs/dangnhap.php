<?php

/**
 * NukeViet Content Management System
 * @version 4.x
 * @author VINADES.,JSC <contact@vinades.vn>
 * @copyright (C) 2009-2021 VINADES.,JSC. All rights reserved
 * @license GNU/GPL version 2 or any later version
 * @see https://github.com/nukeviet The NukeViet CMS GitHub project
 */

if (!defined('NV_IS_MOD_PAGE')) {
    exit('Stop!!!');
}

if($nv_Request->isset_request('login', 'post,get')){
    session_start();
    $sql = "SELECT * FROM nv4_vi_duanmoi_client WHERE email =:email AND mat_khau=:mat_khau";
    $stmt = $db->prepare($sql);
    $stmt->bindParam('email', $_POST['email']);
    $stmt->bindParam('mat_khau', $_POST['mat_khau']);
    $stmt->execute();
    $exe = $stmt->fetch();
    $_SESSION['id'] = $exe['id'];
    // print_r($_SESSION['id'] = $exe['id']);die;
    if($_SESSION['id'] > 0 ){
        die('success');
    }
}

if($nv_Request->isset_request('logout', 'post,get')){
    session_start();
    print_r($_SESSION['id']);die;
}


$xtpl = new XTemplate('dangnhap.html', NV_ROOTDIR . '/themes/' . $global_config['module_theme'] . '/modules/' . $module_file);
$xtpl->parse('main');
$contents = $xtpl->text('main');

include NV_ROOTDIR . '/includes/header.php';
echo nv_site_theme($contents);
include NV_ROOTDIR . '/includes/footer.php';
