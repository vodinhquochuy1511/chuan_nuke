<?php

/**
 * NukeViet Content Management System
 * @version 4.x
 * @author VINADES.,JSC <contact@vinades.vn>
 * @copyright (C) 2009-2021 VINADES.,JSC. All rights reserved
 * @license GNU/GPL version 2 or any later version
 * @see https://github.com/nukeviet The NukeViet CMS GitHub project
 */

if (!defined('NV_IS_MOD_PAGE')) {
    exit('Stop!!!');
}
// include('project.php');

global $user_info;
$databaseName = 'nv4_vi_duanmoi_themmoiduan';
// $arr_name = [
//     'ten_du_an'         => 'Ten du an',
//     'so_tien'           => 'Tien quyen gop',
//     'hinh_anh'          => 'Hinh anh du an',
//     'mo_ta_chi_tiet'    => 'Mo ta chi tiet',
//     'thoi_han'          => 'Thời hạn',
//     'mo_ta_ngan'        => 'Mo ta ngan',
//     'is_open'           => 'Trang thai',
// ];

// $page_title = $lang_module['taoduan'];
$xtpl = new XTemplate('taoduan.html', NV_ROOTDIR . '/themes/' . $global_config['module_theme'] . '/modules/' . $module_file);
//thêm mới dự án
if($nv_Request->isset_request('themmoi','post,get')){
    $tag = explode(',', $_POST['tag']);
    $list_anh = '';
    $link_luu_anh = NV_ROOTDIR . '/' . NV_UPLOADS_DIR . '/' . $module_name;
    for ($i=0; $i < $_POST['so_luong_anh']; $i++) { 
        $upload = new NukeViet\Files\Upload('images', $global_config['forbid_extensions'], $global_config['forbid_mimes'], NV_UPLOAD_MAX_FILESIZE, 1600, 300);
        $upload->setLanguage($lang_global);
        $upload_info = $upload->save_file($_FILES['anh_' . $i] , $link_luu_anh, false, $global_config['nv_auto_resize']);
        if (strlen($list_anh) > 10) {
            $list_anh .= ',' . NV_BASE_SITEURL . '/' . NV_UPLOADS_DIR . '/' .$module_name . '/' . $upload_info['basename'];
        } else {
            $list_anh = NV_BASE_SITEURL . '/' . NV_UPLOADS_DIR . '/' .$module_name . '/' . $upload_info['basename'];
        }
    }
    foreach ($duan as $key => $value) {
        $post[$value] = $_POST[$value];
    }
    $post['hinh_anh'] = $list_anh;
    $post['slug_du_an'] = createSlug($_POST['ten_du_an']);
    $err = request($_POST, $arr_name);
    
    if(!empty($err)){
        foreach($err as $key => $value){
            print_r($value);
        }
        die;
    }else{
        foreach($tag as $key => $value){
            $sql = "SELECT * FROM `nv4_vi_duanmoi_tags` WHERE slug_ten_tag = '". createSlug($value) ."'"  ;
            $res = $db->query($sql);
            $slug = $res->fetch();
            if(empty($slug)){
                $sql = "INSERT INTO `nv4_vi_duanmoi_tags`(ten_tag, slug_ten_tag,created_at, updated_at) 
                VALUES (:ten_tag,:slug_ten_tag,:created_at,:updated_at)";
                $stmt = $db->prepare($sql);
                $stmt->bindParam('slug_ten_tag', createSlug($value));
                $stmt->bindParam('ten_tag', $value);        
                $stmt->bindValue('created_at', NV_CURRENTTIME);        
                $stmt->bindValue('updated_at', 0); 
                $exe = $stmt->execute();
            }         
        }
        $post['id_nguoi_tao'] = $user_info['userid'];
        $exe = store_client($post, $databaseName);
        if($exe){
            die('success');
        }else{
            die('error');
        }   
        // $sql = "INSERT INTO nv4_vi_duanmoi_themmoiduan(ten_du_an, slug_du_an, mo_ta_ngan, so_tien, mo_ta_chi_tiet, hinh_anh, thoi_han, is_open, id_tag, id_nguoi_tao, created_at, updated_at) 
        //         VALUES (:ten_du_an,:slug_du_an,:mo_ta_ngan,:so_tien,:mo_ta_chi_tiet,:hinh_anh,:thoi_han,:is_open,:id_tag,:id_nguoi_tao,:created_at,:updated_at)";
        // $stmt = $db->prepare($sql);
        // $stmt->bindParam('ten_du_an', $_POST['ten_du_an']);        
        // $stmt->bindParam('slug_du_an', $_POST['slug_du_an']);        
        // $stmt->bindParam('mo_ta_ngan', $_POST['mo_ta_ngan']);        
        // $stmt->bindParam('so_tien', $_POST['so_tien']);        
        // $stmt->bindParam('thoi_han', $_POST['thoi_han']);        
        // $stmt->bindParam('mo_ta_chi_tiet', $_POST['mo_ta_chi_tiet']);        
        // $stmt->bindParam('hinh_anh', $_POST['hinh_anh']);        
        // $stmt->bindParam('is_open', $_POST['is_open']);        
        // $stmt->bindParam('id_tag', $_POST['tag']);        
        // $stmt->bindParam('id_nguoi_tao', $_SESSION['id']);        
        // $stmt->bindValue('created_at', NV_CURRENTTIME);        
        // $stmt->bindValue('updated_at', 0);  
        // $exe = $stmt->execute();
        // if($exe){
        //     die('success');
        // }else{
        //     die('error');
        // }    
    }
}
$button  ='<button type="button" id="create" class="btn btn-primary">Thêm mới</button>';
// print_r($_SESSION['id']);die;

if(!$user_info['userid']) {
    $errors = '<script type="text/javascript">toastr.error("Bạn cần Đăng Nhập Vào Hệ Thống!")</script>';
    $button  ='<button type="button" id="create" class="btn btn-primary" disabled>Thêm mới</button>';
    $xtpl->assign('err', $errors);
}

$xtpl->assign('BUTTON', $button);

$xtpl->parse('main');
$contents = $xtpl->text('main');


include NV_ROOTDIR . '/includes/header.php';
echo nv_site_theme($contents);
include NV_ROOTDIR . '/includes/footer.php';
